/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package root.eval1.modelo;

/**
 *
 * @author gvejar
 */
public class Calculadora {
    private Double capital;
    private Double tasa;
    private Double anios;
    private Double interes;

    public Calculadora (String capital, String tasa, String anios) {

        System.out.println("Calculadora capital: "+capital);
        System.out.println("Calculadora tasa: "+tasa);
        System.out.println("Calculadora anios: "+anios);

        this.capital = Double.parseDouble(capital);
        this.tasa = Double.parseDouble(tasa);
        this.anios = Double.parseDouble(anios);
        
    }

    public Double calcularInteres () {
        return this.capital * (this.tasa/100) * this.anios;
    }

    public Double getCapital() {
        return this.capital;
    }

    public Double getTasa() {
        return this.tasa;
    }

    public Double getAnios() {
        return this.anios;
    }

}
