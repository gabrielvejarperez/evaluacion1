<%-- 
    Document   : index
    Created on : 23-09-2021, 21:16:08
    Author     : gvejar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interes Simple</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <h1>Calculo de Interés Simple</h1>
        <form action="InteresController" method="POST">
            
            <div class="form-group">
              <label for="capital">Capital</label>
              <input type="number" class="form-control" id="capital" name="capital" placeholder="Ingresa capital">
            </div>
            <div class="form-group">
              <label for="tasa">Tasa de interes</label>
              <input type="number" class="form-control" id="tasa" name="tasa" placeholder="Ingresa tasa de interés">
            </div>            
            <div class="form-group">
              <label for="anios">Años</label>
              <input type="number" class="form-control" id="anios" name="anios" placeholder="Ingresa plazo en años">
            </div>            
            
            <div><button type="submit" class="btn btn-default">Calcular</button></div>
            
        </form>
    </body>
</html>
