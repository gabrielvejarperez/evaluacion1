<%-- 
    Document   : resultado
    Created on : 25-09-2021, 22:18:33
    Author     : gveja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="root.eval1.modelo.Calculadora" %>
<!DOCTYPE html>

<% 
    Calculadora calc = (Calculadora)request.getAttribute("calculadora");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado calculo</title>
    </head>
    <body>
        <h1> 
            Capital: $<%=calc.getCapital()%>
        </h1>
        <h1> 
            Tasa: <%=calc.getTasa()%>
        </h1>
        <h1> 
            Plazo(años): <%=calc.getAnios()%>
        </h1>
        <h1> 
            Interés generado: $<%=calc.calcularInteres()%>
        </h1>
    </body>
</html>
